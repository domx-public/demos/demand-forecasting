# demand-forecasting

DOMX offers a cost-effective and universal retrofit IoT solution that integrates seamlessly with legacy natural gas boilers, providing for optimized space heating and circumventing the need to install dedicated metering equipment for consumption monitoring. The DOMX heating controller builds on the widely applicable Opentherm protocol, to optimally manage the boiler operation, by considering the users’ comfort limits as set by the room thermostat, and local climate conditions as captured by device sensors. The DOMX smartphone application enables gas consumers to understand how energy is consumed and to achieve long-term consumer behavior change through nudging interventions. The edge devices connect over Wi-Fi to the Internet and the company’s cloud energy management platform, enabling adaptation to building characteristics (envelope, boiler), climate variations (indoor, outdoor) and user schedules-preferences.

![DOMX](image.png)


DOMX has developed a ML-based framework that produces the digital twin representation of natural gas boilers (as synthetic time-series), emulating their operation under different configurations. This is the home of DOMX demand forecasting demo which uses the pre-trained model for HOME2.

The demo can be used to forecast any selected day that is included in the experimental dataset of HOME2.

## Usage
- Clone the repository
- Open a linux terminal:

```
$ cd dist
$ ./HOME2_demo <dataset_path> <forecast_day>
```
- Positional arguments:
    - dataset_path: enter full path of dataset file in .csv format
    - forecast_day: enter day to forecast, format: YY-mm-DD, e.g. 2022-02-18
- Example:

```
$ ./HOME2_demo ../data/HOME2.csv 2022-02-07
```
> forecast_day must be a valid date between 2022-02-06 and 2022-03-06

## Dataset

The DOMX smart heating controller continuously collects indoor ambient data and data from the outdoor environment as well as data produced by the boiler’s activity, including indoor and outdoor temperatures, indoor temperature setpoint, boiler water temperatures and setpoint and the boiler load level among other metrics.

Currently data from five residential consumers (HOME2, HOME9, HOME13, HOME38, HOME42) are available under the /data folder of this repo. The experimental dataset is vailable in .csv format and contains one month of data in 1 min temporal resolution from 2022-02-06 to 2022-03-06. The following metrics are included:

- **time**: timestamp in YY-mm-DD HH:MM:SS format
- **blr_mod_lvl**: boiler load level expressed as a percentage of maximum boiler modulation capacity
- **water**: binary variable which indicates whether domestic hot water is requested
- **blr_t**: the current temperature of the water inside the boiler
- **t_ret**: the current temperature of the water entering the boiler from the radiators
- **t_out**: the current outdoor temperature
- **t_r**: the current indoor temperature
- **t_set**: boiler setpoint - the target water temperature the boiler is instructed to reach
- **t_r_set**: indoor setpoint - the target room temperature the heating system must reach
- **bypass**: binary variable which indicates whether the adaptive heating algorithm is active or not. In case this is 0, the legacy mode of the boiler is active which always sets the boiler setpoint to a fixed temperature, typically within the range of 65-80°C
- **otc_maxt**: optmized boiler setpoint which is instructed when adaptive heating is active